
export interface dishModel {
  name: string
  img: {
    src: string,
    alt: string
  }
  description: string,
  ingredients: string[],
  flag: string
}



